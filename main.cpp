#include <iostream>
#include "Dictionary.h"
#include "KeyValuePair.h"


using namespace std;

int main() {

    Dictionary<string, int> SIdictionary;
    for (int i = 0; i < 20; i++)
    {
        SIdictionary.Add(KeyValuePair<string, int>("Age",i));
    }
    SIdictionary.PrintDictionary();
    cout << endl << endl;
    Dictionary<string, char> SCdictionary;
    for (int i = 21; i < 40; i++)
    {
        SCdictionary.Add(KeyValuePair<string, char>("Age",(char)i));
    }
    SCdictionary.PrintDictionary();
    cout << endl << endl;
    Dictionary<int, char> ICdictionary;
    for (int i = 41; i < 60; i++)
    {
        ICdictionary.Add(KeyValuePair<int, char>(i,(char)i));
    }
    ICdictionary.PrintDictionary();


    return 0;
}