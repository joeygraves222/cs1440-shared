//
// Created by Joseph Graves on 4/4/17.
//

#ifndef DICTIONARY_DICTIONARYTESTER_H
#define DICTIONARY_DICTIONARYTESTER_H


class DictionaryTester
{
public:
    void TestConstructor();
    void TestFind();
    void TestFindAt();
    void TestAdd();
    void TestGetCount();
};


#endif //DICTIONARY_DICTIONARYTESTER_H
