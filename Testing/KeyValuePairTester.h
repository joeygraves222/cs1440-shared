//
// Created by Joseph Graves on 4/4/17.
//

#ifndef DICTIONARY_KEYVALUEPAIRTESTER_H
#define DICTIONARY_KEYVALUEPAIRTESTER_H


class KeyValuePairTester
{
public:
    void TestConstructor();
    void TestGetKey();
    void TestGetValue();
    void TestSetKey();
    void TestSetValue();
};


#endif //DICTIONARY_KEYVALUEPAIRTESTER_H
