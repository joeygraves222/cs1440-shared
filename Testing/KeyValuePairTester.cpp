//
// Created by Joseph Graves on 4/4/17.
//

#include "KeyValuePairTester.h"
#include "../KeyValuePair.h"

void KeyValuePairTester::TestConstructor()
{
    cout << "Testing Constructor..." << endl;
    try
    {
        KeyValuePair<string, string> test1("Age", "5ft, 6in");
        KeyValuePair<string, int> test2("Age", 66);
        KeyValuePair<int, string> test3(23, "5ft, 6in");
        KeyValuePair<int, int> test4(24, 66);
        cout << "Successful!" << endl;
    }
    catch (exception ex)
    {
        cout << "Failed" << endl;
    }
}

void KeyValuePairTester::TestGetKey()
{
    cout << "Testing GetKey()..." << endl;
    KeyValuePair<string, string> test1("Age", "5ft, 6in");
    string test = "Age";
    if (test == test1.GetKey())
    {
        cout << "Successfull!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void KeyValuePairTester::TestGetValue()
{
    cout << "Testing GetValue()..." << endl;
    KeyValuePair<string, string> test1("Age", "5ft, 6in");
    string test = "5ft, 6in";
    if (test == test1.GetValue())
    {
        cout << "Successfull!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void KeyValuePairTester::TestSetKey()
{
    cout << "Testing SetKey()..." << endl;
    KeyValuePair<string, string> test1("Age", "5ft, 6in");
    string test = "Name";
    test1.SetKey("Name");
    if (test == test1.GetKey())
    {
        cout << "Successfull!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void KeyValuePairTester::TestSetValue()
{
    cout << "Testing SetValue..." << endl;
    KeyValuePair<string, string> test1("Age", "5ft, 6in");
    string test = "Age";
    test1.SetValue("Age");
    if (test == test1.GetValue())
    {
        cout << "Successfull!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}
