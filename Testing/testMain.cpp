//
// Created by Joseph Graves on 4/3/17.
//

#include <iostream>
#include "DictionaryTester.h"
#include "KeyValuePairTester.h"



using namespace std;

int main() {

    DictionaryTester testDict;
    cout << "Testing Dictionary..." << endl;
    testDict.TestConstructor();
    testDict.TestAdd();
    testDict.TestGetCount();
    testDict.TestFindAt();
    testDict.TestFind();
    cout << endl << endl;
    cout << "Testing KeyValuePair..." << endl;
    KeyValuePairTester TesterPair;
    TesterPair.TestConstructor();
    TesterPair.TestSetKey();
    TesterPair.TestSetValue();
    TesterPair.TestGetKey();
    TesterPair.TestGetValue();


    return 0;
}