//
// Created by Joseph Graves on 4/4/17.
//

#include "DictionaryTester.h"
#include "../Dictionary.h"

void DictionaryTester::TestConstructor()
{
    cout << "Testing Constructor..." << endl;
    try
    {
        Dictionary<string, int> TestDictionary(35);
        Dictionary<string, string> Test1(35);
        Dictionary<int, string> Test2(35);
        Dictionary<int, int> Test3(35);
        cout << "Successful!" << endl;
    }
    catch (exception ex)
    {
        cout << "Failed" << endl;
    }

}

void DictionaryTester::TestFind()
{
    cout << "Testing Find()..." << endl;
    Dictionary<string, int> TestDictionary(35);
    //cout << "Searching for key \"Height\"..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Age", 24));
    TestDictionary.Add(KeyValuePair<string, int>("Height",66));
    TestDictionary.Add(KeyValuePair<string, int>("Weight", 155));
    TestDictionary.Add(KeyValuePair<string, int>("Siblings", 11));
    //TestDictionary.PrintDictionary();
    //cout << "Printing item with key \"Height\"..." << endl;
    //TestDictionary.GetItem("Height").Print();
    if (TestDictionary.GetItem("Height").GetKey() == "Height")
    {
        cout << "Successful!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void DictionaryTester::TestFindAt()
{
    cout << "Testing FindAt()..." << endl;
    Dictionary<string, int> TestDictionary(35);
    //cout << "Searching at index 2..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Age", 24));
    TestDictionary.Add(KeyValuePair<string, int>("Height",66));
    TestDictionary.Add(KeyValuePair<string, int>("Weight", 155));
    TestDictionary.Add(KeyValuePair<string, int>("Siblings", 11));
    //TestDictionary.PrintDictionary();
    //cout << "Printing item at index 2..." << endl;
    //TestDictionary.GetItemAt(2).Print();
    if (TestDictionary.GetItemAt(2).GetKey() == "Weight" && TestDictionary.GetItemAt(2).GetValue() == 155)
    {
        cout << "Successful!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void DictionaryTester::TestAdd()
{
    cout << "Testing Add()..." << endl;
    Dictionary<string, int> TestDictionary(35);
    //cout << "Adding..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Age", 24));
    //cout << "Adding..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Height",66));
    //cout << "Adding..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Weight", 155));
    //cout << "Adding..." << endl;
    TestDictionary.Add(KeyValuePair<string, int>("Siblings", 11));
    //TestDictionary.PrintDictionary();
    if (TestDictionary.GetCount() == 4)
    {
        cout << "Successful!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}

void DictionaryTester::TestGetCount()
{
    cout << "Testing GetCount()..." << endl;
    Dictionary<string, int> TestDictionary(35);
    TestDictionary.Add(KeyValuePair<string, int>("Age", 24));
    TestDictionary.Add(KeyValuePair<string, int>("Height",66));
    TestDictionary.Add(KeyValuePair<string, int>("Weight", 155));
    TestDictionary.Add(KeyValuePair<string, int>("Siblings", 11));
    if (TestDictionary.GetCount() == 4)
    {
        cout << "Successful!" << endl;
    }
    else
    {
        cout << "Failed" << endl;
    }
}
