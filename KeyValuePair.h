//
// Created by Joseph Graves on 4/3/17.
//

#ifndef DICTIONARY_KEYVALUEPAIR_H
#define DICTIONARY_KEYVALUEPAIR_H

#include <form.h>
#include <iostream>

using namespace std;

template <typename T, typename C>
class KeyValuePair {
public:
    KeyValuePair(T key, C value);
    KeyValuePair();

    void SetKey(T key);

    void SetValue(C value);

    T GetKey();

    C GetValue();

    void Print();

private:
    T Key;
    C Value;
};

template <typename T, typename C>
KeyValuePair<T,C>::KeyValuePair(T key, C value)
{
    Key = key;
    Value = value;
};

template <typename T, typename C>
KeyValuePair<T, C>::KeyValuePair()
{

};

template <typename T, typename C>
void KeyValuePair<T,C>::SetKey(T key)
{
    Key = key;
}


template <typename T, typename C>
void KeyValuePair<T,C>::SetValue(C value)
{
    Value = value;
}


template <typename T, typename C>
T KeyValuePair<T,C>::GetKey()
{
    return Key;
}


template <typename T, typename C>
C KeyValuePair<T,C>::GetValue()
{
    return Value;
}


template <typename T, typename C>
void KeyValuePair<T,C>::Print()
{
    cout << "|" << Key << ": " << Value << endl;
}



#endif //DICTIONARY_KEYVALUEPAIR_H
