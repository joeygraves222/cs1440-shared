//
// Created by Joseph Graves on 4/3/17.
//

#ifndef DICTIONARY_DICTIONARY_H
#define DICTIONARY_DICTIONARY_H

#include "KeyValuePair.h"
#include <iostream>
#include <vector>
using namespace std;

template <typename T, typename C>
class Dictionary
{
public:
    Dictionary(int size);
    Dictionary();
    KeyValuePair<T, C>* DictionaryList;
    void Add(KeyValuePair<T, C> item);
    void PrintDictionary();
    KeyValuePair<T, C> GetItemAt(int index);
    KeyValuePair<T, C> GetItem(T key);
    int GetCount(){return DictionarySize;}


private:
    void Resize(int currentSize);
    int MaxSize;
    int DictionarySize;
};

template <typename T, typename C>
Dictionary<T, C>::Dictionary()
{
    MaxSize = 100;
    DictionaryList = new KeyValuePair<T, C>[MaxSize];
    DictionarySize = 0;
}

template <typename T, typename C>
Dictionary<T, C>::Dictionary(int size)
{
    MaxSize = size;
    DictionaryList = new KeyValuePair<T, C>[size];
    DictionarySize = 0;
}

template <typename T, typename C>
void Dictionary<T, C>::Add(KeyValuePair<T, C> item)
{
    if (DictionarySize == (MaxSize - 10))
    {
        Resize(MaxSize);
    }
    DictionaryList[DictionarySize].SetKey(item.GetKey());
    DictionaryList[DictionarySize].SetValue(item.GetValue());
    DictionarySize++;
}


template <typename T, typename C>
void Dictionary<T, C>::PrintDictionary()
{
    for (int i = 0; i < DictionarySize; i++)
    {
        DictionaryList[i].Print();
    }
}

template <typename T, typename C>
KeyValuePair<T, C> Dictionary<T, C>::GetItemAt(int index)
{

    if (index >= DictionarySize)
    {
        throw std::invalid_argument( "Item not found" );
    }
    else
    {
        return DictionaryList[index];
    }
}

template <typename T, typename C>
KeyValuePair<T, C> Dictionary<T, C>::GetItem(T key)
{

    for (int i = 0; i < DictionarySize; i++)
    {
        if (DictionaryList[i].GetKey() == key)
        {
            return DictionaryList[i];
        }
    }
    throw std::invalid_argument( "Item not found" );

}

template <typename T, typename C>
void Dictionary<T, C>::Resize(int currentSize)
{
    KeyValuePair<T, C>* temp = new KeyValuePair<T, C>[currentSize*2];
    for (int i = 0; i < currentSize; i++)
    {
        temp[i].SetKey(DictionaryList[i].GetKey());
        temp[i].SetValue(DictionaryList[i].GetValue());
    }
}

#endif //DICTIONARY_DICTIONARY_H
